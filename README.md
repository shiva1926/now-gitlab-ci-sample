## Aim

This repository demonstrates how to replicate the now for gitlab integration features using gitlab ci

## Why bother

This is useful if you want to have different set of environment variables which means you can't use the default now for gitlab integration because you need to customize the now command to use a custom now.json file.

However a naive approach to this problem leads to a project where all the deployments are made by the same person and where you loose the deployment metadata (such as the associated commit hash and links to navigate there).

## Current advancement 
The sample project mostly solves this issue 

- build metadata is properly set including avatar icon
- build-env variables are set as documented at https://zeit.co/docs/v2/advanced/now-for-gitlab/#
- for merge requests, once the deployment is over, a comment is added to the MR with the deployment url

## Requirements

For this pipeline to work, you will have to set the following environment variables: 
- NOW_TOKEN
- NOW_API_URL (should be https://api.zeit.co/)